..	_helpers:

Tabbed Navigation
*****************

To make effective use of screen space, it often makes sense to sibling concepts into a tabbed navigation area. The
:class:`Tabs` class helps to implement this easily. Simply create instances of the :class:`Tabs` class when you
register event listeners for your UI element and the instance will do the rest of the work for you.

..  autoclass:: Tabs
    :members:
