��!0      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�..      _templating:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��
templating�u�tagname�h	�line�K�parent�hhh�source��+D:\Ember\app\docs\pages\apps\templating.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Templating and Handlebars�h]�h �Text����Templating and Handlebars�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(h��HTML templating in Foundry Virtual Tabletop uses the very flexible and easy-to-use Handlebars.js as it's templating
engine. Documentation for Handlebars.js is available here: https://handlebarsjs.com/�h]�(h.��HTML templating in Foundry Virtual Tabletop uses the very flexible and easy-to-use Handlebars.js as it’s templating
engine. Documentation for Handlebars.js is available here: �����}�(h��HTML templating in Foundry Virtual Tabletop uses the very flexible and easy-to-use Handlebars.js as it's templating
engine. Documentation for Handlebars.js is available here: �hh;hhh NhNubh �	reference���)��}�(h�https://handlebarsjs.com/�h]�h.�https://handlebarsjs.com/�����}�(hhhhFubah}�(h]�h]�h]�h]�h]��refuri�hHuhhDhh;ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h��The basic template rendering workflow is that the :class:`Application` defines it's own `getData()` method which vends
an object of data suitable for use in templating. For example, suppose an application defined the following:�h]�(h.�2The basic template rendering workflow is that the �����}�(h�2The basic template rendering workflow is that the �hh[hhh NhNub�sphinx.addnodes��pending_xref���)��}�(h�:class:`Application`�h]�h �literal���)��}�(hhih]�h.�Application()�����}�(hhhhmubah}�(h]�h]�(�xref��js��js-class�eh]�h]�h]�uhhkhhgubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�hx�refexplicit���	js:object�N�	js:module�N�	reftarget��Application��refdoc��pages/apps/templating��refwarn��uhheh h!hK	hh[ubh.� defines it’s own �����}�(h� defines it's own �hh[hhh NhNubh �title_reference���)��}�(h�`getData()`�h]�h.�	getData()�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh[ubh.�� method which vends
an object of data suitable for use in templating. For example, suppose an application defined the following:�����}�(h�� method which vends
an object of data suitable for use in templating. For example, suppose an application defined the following:�hh[hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK	hh$hhubh �literal_block���)��}�(h�egetData() {
  return {
    name: "Edgar",
    mood: "curious",
    items: ["one", "foo", "bar"]
  }
}�h]�h.�egetData() {
  return {
    name: "Edgar",
    mood: "curious",
    items: ["one", "foo", "bar"]
  }
}�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��language��
javascript��linenos���highlight_args�}�uhh�h h!hKhh$hhubh:)��}�(h��When defining an HTML template, these variables will be available to use through the embedded handlebars expressions
described in the above documentation. A simple HTML template might look something like:�h]�h.��When defining an HTML template, these variables will be available to use through the embedded handlebars expressions
described in the above documentation. A simple HTML template might look something like:�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh�)��}�(hX  <div>
    <h3>My Name is {{name}}</h3>
    <p>I am feeling very {{mood}} today to learn more about FVTT templating.</p>
    <p>I have several items ready to go:</p>
    <ol>
        {{#each items}}
        <li>{{this}}</li>
        {{/each}}
    </ol>
</div>�h]�h.X  <div>
    <h3>My Name is {{name}}</h3>
    <p>I am feeling very {{mood}} today to learn more about FVTT templating.</p>
    <p>I have several items ready to go:</p>
    <ol>
        {{#each items}}
        <li>{{this}}</li>
        {{/each}}
    </ol>
</div>�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�h�h�h��html�hh�}�uhh�h h!hKhh$hhubeh}�(h]�(�templating-and-handlebars�heh]�h]�(�templating and handlebars��
templating�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�h�hs�expect_referenced_by_id�}�hhsubh#)��}�(hhh]�(h()��}�(h�Core Template Helpers�h]�h.�Core Template Helpers�����}�(hh�hh�hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh�hhh h!hK'ubh:)��}�(h�oThe core FVTT modules provides several built-in template helpers which provide commonly used utility functions.�h]�h.�oThe core FVTT modules provides several built-in template helpers which provide commonly used utility functions.�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK)hh�hhubh#)��}�(hhh]�(h()��}�(h�FilePicker UI�h]�h.�FilePicker UI�����}�(hj  hj  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj  hhh h!hK,ubh:)��}�(hXl  You can render a file-picker UI element by calling ``{{filePicker target=String type=String}`` with the required field
``target`` which provides the ``name`` of the ``<input>`` element which should be modified by the FilePicker UI and
``type`` which currently accepts either "image" or "audio" as a way to filter the set of files which are shown.
Example usage is:�h]�(h.�3You can render a file-picker UI element by calling �����}�(h�3You can render a file-picker UI element by calling �hj   hhh NhNubhl)��}�(h�+``{{filePicker target=String type=String}``�h]�h.�'{{filePicker target=String type=String}�����}�(hhhj)  ubah}�(h]�h]�h]�h]�h]�uhhkhj   ubh.� with the required field
�����}�(h� with the required field
�hj   hhh NhNubhl)��}�(h�
``target``�h]�h.�target�����}�(hhhj<  ubah}�(h]�h]�h]�h]�h]�uhhkhj   ubh.� which provides the �����}�(h� which provides the �hj   hhh NhNubhl)��}�(h�``name``�h]�h.�name�����}�(hhhjO  ubah}�(h]�h]�h]�h]�h]�uhhkhj   ubh.� of the �����}�(h� of the �hj   hhh NhNubhl)��}�(h�``<input>``�h]�h.�<input>�����}�(hhhjb  ubah}�(h]�h]�h]�h]�h]�uhhkhj   ubh.�; element which should be modified by the FilePicker UI and
�����}�(h�; element which should be modified by the FilePicker UI and
�hj   hhh NhNubhl)��}�(h�``type``�h]�h.�type�����}�(hhhju  ubah}�(h]�h]�h]�h]�h]�uhhkhj   ubh.�� which currently accepts either “image” or “audio” as a way to filter the set of files which are shown.
Example usage is:�����}�(h�y which currently accepts either "image" or "audio" as a way to filter the set of files which are shown.
Example usage is:�hj   hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK.hj  hhubh�)��}�(h��<input type="text" name="profilePath" title="Profile Image Path" value={{profile}} placeholder="File Path"/>
{{filePicker target="profilePath" type="image"}}�h]�h.��<input type="text" name="profilePath" title="Profile Image Path" value={{profile}} placeholder="File Path"/>
{{filePicker target="profilePath" type="image"}}�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�h�h�h��html�hh�}�uhh�h h!hK3hj  hhubeh}�(h]��filepicker-ui�ah]�h]��filepicker ui�ah]�h]�uhh"hh�hhh h!hK,ubh#)��}�(hhh]�(h()��}�(h�Rich Text Editor (TinyMCE)�h]�h.�Rich Text Editor (TinyMCE)�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hj�  hhh h!hK9ubh:)��}�(hX,  Another commonly useful helper is to render a rich text editor field which allows you to use the TinyMCE editor for
more user-friendly text editing by calling the ``{editor target=key content=data button=true}}`` helper where the
``target`` argument accepts the key name of the data element which should be updated when text is modified, ``content``
specifies the current value of the text pre-loaded in the editor, and ``button`` controls whether the editor is activated
through a toggled button (if true) or is always active (if false). Example usage is:�h]�(h.��Another commonly useful helper is to render a rich text editor field which allows you to use the TinyMCE editor for
more user-friendly text editing by calling the �����}�(h��Another commonly useful helper is to render a rich text editor field which allows you to use the TinyMCE editor for
more user-friendly text editing by calling the �hj�  hhh NhNubhl)��}�(h�1``{editor target=key content=data button=true}}``�h]�h.�-{editor target=key content=data button=true}}�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhkhj�  ubh.� helper where the
�����}�(h� helper where the
�hj�  hhh NhNubhl)��}�(h�
``target``�h]�h.�target�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhkhj�  ubh.�b argument accepts the key name of the data element which should be updated when text is modified, �����}�(h�b argument accepts the key name of the data element which should be updated when text is modified, �hj�  hhh NhNubhl)��}�(h�``content``�h]�h.�content�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhkhj�  ubh.�G
specifies the current value of the text pre-loaded in the editor, and �����}�(h�G
specifies the current value of the text pre-loaded in the editor, and �hj�  hhh NhNubhl)��}�(h�
``button``�h]�h.�button�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhkhj�  ubh.�~ controls whether the editor is activated
through a toggled button (if true) or is always active (if false). Example usage is:�����}�(h�~ controls whether the editor is activated
through a toggled button (if true) or is always active (if false). Example usage is:�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK;hj�  hhubh�)��}�(h�t<div class="container">
    {{editor content=data.biography.value target="data.biography.value" button=true}}
</div>�h]�h.�t<div class="container">
    {{editor content=data.biography.value target="data.biography.value" button=true}}
</div>�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�h�h�h��html�hh�}�uhh�h h!hKAhj�  hhubeh}�(h]��rich-text-editor-tinymce�ah]�h]��rich text editor (tinymce)�ah]�h]�uhh"hh�hhh h!hK9ubeh}�(h]��core-template-helpers�ah]�h]��core template helpers�ah]�h]�uhh"hhhhh h!hK'ubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jU  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(h�hh�h�j/  j,  j�  j�  j'  j$  u�	nametypes�}�(h�h�Nj/  Nj�  Nj'  Nuh}�(hh$h�h$j,  h�j�  j  j$  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.�0Hyperlink target "templating" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj�  uba�transformer�N�
decoration�Nhhub.